package ansiseq

import "fmt"

const (
  vSchwarz      = "\x1b[30m"
  vRot          = "\x1b[31m"
  vGruen        = "\x1b[32m"
  vGelb         = "\x1b[33m"
  vBlau         = "\x1b[34m"
  vViolett      = "\x1b[35m"
  vHellblau     = "\x1b[36m"
  vWeiss        = "\x1b[37m"

  hSchwarz      = "\x1b[40m"
  hRot          = "\x1b[41m"
  hGruen        = "\x1b[42m"
  hGelb         = "\x1b[43m"
  hBlau         = "\x1b[44m"
  hViolett      = "\x1b[45m"
  hHellblau     = "\x1b[46m"
  hWeiss        = "\x1b[47m"

  normal        = "\x1b[0m"
  fett          = "\x1b[1m"
  unterstrichen = "\x1b[2m"
  blinkend      = "\x1b[3m"

  reset         = "\x1b[0;49m"
  loeschen      = "\x1b[2J"
)

func VFarbeSetzen (f Farbe) {
  switch f {
    case Schwarz:   fmt.Printf (vSchwarz)
    case Rot:       fmt.Printf (vRot)
    case Gruen:     fmt.Printf (vGruen)
    case Gelb:      fmt.Printf (vGelb)
    case Blau:      fmt.Printf (vBlau)
    case Violett:   fmt.Printf (vViolett)
    case Hellblau:  fmt.Printf (vHellblau)
    case Weiss:     fmt.Printf (vWeiss)
  }
}

func HFarbeSetzen (f Farbe) {
  switch f {
    case Schwarz:   fmt.Printf (hSchwarz)
    case Rot:       fmt.Printf (hRot)
    case Gruen:     fmt.Printf (hGruen)
    case Gelb:      fmt.Printf (hGelb)
    case Blau:      fmt.Printf (hBlau)
    case Violett:   fmt.Printf (hViolett)
    case Hellblau:  fmt.Printf (hHellblau)
    case Weiss:     fmt.Printf (hWeiss)
  }
}

func EffektSetzen (e Effekt) {
  switch e {
    case Normal:        fmt.Printf (normal)
    case Fett:          fmt.Printf (fett)
//    case Unterstrichen: fmt.Printf (unterstrichen)
//    case Blinkend:      fmt.Printf (blinkend)
  }
}

func TextattributeSetzen (vg, hg Farbe, e Effekt) {
  VFarbeSetzen (vg)
  HFarbeSetzen (hg)
  EffektSetzen (e)
}

func AllesZuruecksetzen () {
  fmt.Printf (reset)
}

func SetzePosition (zeile int, spalte int) {
  fmt.Printf ("\x1b[%d;%dH", zeile, spalte)
}

func LoescheBildschirm () {
  fmt.Printf (loeschen)
  SetzePosition (0, 0)
}
