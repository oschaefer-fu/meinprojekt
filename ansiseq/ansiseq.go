package ansiseq

//  Autor:  Oliver Schäfer
//  Datum:  24.03.2014
//  Zweck:  Einfache Texteffekte in der Konsole
//          mit Hilfe von ANSI-Sequenzen

type Farbe byte; const (
  Schwarz = Farbe (iota)
  Rot
  Gruen
  Gelb
  Blau
  Violett
  Hellblau
  Weiss
)

type Effekt byte; const (
  Normal = Effekt (iota)
  Fett
  //  Unterstrichen  // steht nicht zur Verfügung
  //  Blinkend  //  steht nicht zur Verfügung
)

// func VFarbeSetzen (f Farbe)

// func HFarbeSetzen (f Farbe)

// func EffektSetzen (e Effekt)

// func TextattributeSetzen (vg, hg Farbe, e Effekt)

// func AllesZuruecksetzen ()

// func SetzePosition (zeile int, spalte int)

// func LoescheBildschirm ()
